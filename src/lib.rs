#![deny(missing_docs)]
/*!
See the [description](https://crates.io/crates/pns)
**/

mod core;
mod sys;

pub use core::{Pid, StateInitializationError, Tid};
pub use sys::types::{FireChanges, Net, Node, State, TransitionList, TransitionView};
