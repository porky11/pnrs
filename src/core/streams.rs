use super::ids::Id;

use data_stream::{FromStream, ToStream, from_stream, numbers::EndianSettings, to_stream};

use std::io::{Read, Result, Write};

impl<T, S: EndianSettings> ToStream<S> for Id<T> {
    fn to_stream<W: Write>(&self, stream: &mut W) -> Result<()> {
        to_stream::<S, _, _>(&(self.index as u32), stream)
    }
}

impl<T, S: EndianSettings> FromStream<S> for Id<T> {
    fn from_stream<R: Read>(stream: &mut R) -> Result<Self> {
        let n: u32 = from_stream::<S, _, _>(stream)?;
        Ok(Self::new(n as usize))
    }
}
