use std::{
    fs::File,
    io::{Error, Read},
    path::Path,
};

pub(super) fn read_values(filename: &Path) -> Result<Vec<u32>, Error> {
    let size = (std::fs::metadata(filename)?.len() + 3) / 4;

    let mut file = File::open(filename)?;

    (0..size)
        .map(|_| {
            let mut value = [0; 4];
            file.read_exact(&mut value)?;
            Ok(u32::from_le_bytes(value))
        })
        .collect()
}
