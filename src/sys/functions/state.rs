use crate::{
    FireChanges, TransitionList, TransitionView,
    sys::types::{Net, State},
};

unsafe extern "C" {
    pub(crate) fn pnsCreateState(state: *mut State, net: *const Net);
    pub(crate) fn pnsCloneState(state_clone: *mut State, state: *const State, net: *const Net);
    pub(crate) fn pnsLoadState(
        state: *mut State,
        net: *const Net,
        count: usize,
        values: *const u32,
    ) -> bool;
    pub(crate) fn pnsDestroyState(state: *mut State);

    pub(crate) fn pnsState_transitions<'a>(state: *const State) -> TransitionView<'a>;
    pub(crate) fn pnsState_transitions_backward<'a>(state: *const State) -> TransitionView<'a>;
    pub(crate) fn pnsState_changedTransitions(state: *mut State) -> FireChanges;
    pub(crate) fn pnsState_changedTransitions_backward(state: *mut State) -> FireChanges;

    pub(crate) fn pnsState_fire(state: *mut State, net: *const Net, tid: usize);
    pub(crate) fn pnsState_fire_backward(state: *mut State, net: *const Net, tid: usize);

    pub(crate) fn pnsState_refresh(state: *mut State, net: *const Net);

    pub(crate) fn pnsTransitionView_next(view: *mut TransitionView, tid: *mut usize) -> bool;
    pub(crate) fn pnsDestroyTransitionList(list: *mut TransitionList);
    pub(crate) fn pnsTransitionList_next(list: *mut TransitionList, tid: *mut usize) -> bool;
}
