use crate::sys::types::Net;

unsafe extern "C" {
    pub(crate) fn pnsCreateNet(net: *mut Net);
    pub(crate) fn pnsCloneNet(net_clone: *mut Net, net: *const Net);
    pub(crate) fn pnsLoadNet(net: *mut Net, count: usize, values: *const u32) -> bool;
    pub(crate) fn pnsDestroyNet(net: *mut Net);

    pub(crate) fn pnsNet_serializeSize(net: *const Net) -> usize;
    pub(crate) fn pnsNet_serialize(net: *const Net, values: *mut u32);
    pub(crate) fn pnsNet_addPlace(net: *mut Net) -> usize;
    pub(crate) fn pnsNet_addTransition(net: *mut Net) -> usize;
    pub(crate) fn pnsNet_removePlace(net: *mut Net, pid: usize);
    pub(crate) fn pnsNet_removeTransition(net: *mut Net, tid: usize);

    pub(crate) fn pnsNet_connectPlaceToTransition(net: *mut Net, pid: usize, tid: usize) -> bool;
    pub(crate) fn pnsNet_connectTransitionToPlace(net: *mut Net, tid: usize, pid: usize) -> bool;
    pub(crate) fn pnsNet_disconnectPlaceToTransition(net: *mut Net, pid: usize, tid: usize);
    pub(crate) fn pnsNet_disconnectTransitionToPlace(net: *mut Net, tid: usize, pid: usize);

    pub(crate) fn pnsNet_addInitialTokens(net: &mut Net, pid: usize, count: usize) -> usize;

    pub(crate) fn pnsNet_addConnectedPlace(
        net: *mut Net,
        input_count: usize,
        input_tids: *const usize,
        output_count: usize,
        output_tids: *const usize,
    ) -> usize;
    pub(crate) fn pnsNet_addConnectedTransition(
        net: *mut Net,
        input_count: usize,
        input_pids: *const usize,
        output_count: usize,
        output_pids: *const usize,
    ) -> usize;

    pub(crate) fn pnsNet_duplicatePlace(net: &mut Net, pid: usize) -> usize;
    pub(crate) fn pnsNet_duplicateTransition(net: &mut Net, tid: usize) -> usize;
}
