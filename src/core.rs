mod ids;
mod net;
mod state;
mod streams;
mod utils;

pub use ids::{Pid, Tid};
pub use state::StateInitializationError;

use crate::sys::types::Node;

impl<Id> Node<Id> {
    /// Get a slice of indices to the next nodes.
    pub fn next(&self) -> &[Id] {
        if self.next_count == 0 {
            &[]
        } else {
            unsafe { std::slice::from_raw_parts(self.next, self.next_count) }
        }
    }

    /// Get a slice of indices to the previous nodes.
    pub fn prev(&self) -> &[Id] {
        if self.prev_count == 0 {
            &[]
        } else {
            unsafe { std::slice::from_raw_parts(self.prev, self.prev_count) }
        }
    }
}
