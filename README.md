# Petri Net Simulator for Rust

This is a rust binding to [my petri net simulator written in C](https://gitlab.com/porky11/pns).
It has almost direct mappings to the [C API](https://gitlab.com/porky11/pns), but uses a few Rust features like methods and custom types.

The example should do exactly the same as in the C version.

For more information look at the [documentation](https://docs.rs/pns/) or have a look at the [C API](https://gitlab.com/porky11/pns).

Also have a look at the [petri net editor](https://docs.rs/pn-editor/), which makes use of the whole API, and helps you understand, how it works.

